//
//  Swap.h
//  CookieCrunch
//
//  Created by Ryan Salton on 20/10/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//
#import <Foundation/Foundation.h>
@class Cookie;

@interface Swap : NSObject

@property (nonatomic, strong) Cookie *cookieA;
@property (nonatomic, strong) Cookie *cookieB;

@end
