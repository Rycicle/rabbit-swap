//
//  Level.h
//  CookieCrunch
//
//  Created by Ryan Salton on 20/10/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "Cookie.h"
#import "Tile.h"
#import "Swap.h"
#import "Chain.h"

static const NSInteger NumColumns = 9;
static const NSInteger NumRows = 9;

@interface Level : NSObject

@property (nonatomic, assign) NSUInteger targetScore;
@property (nonatomic, assign) NSUInteger maximumMoves;

-(NSArray *)fillHoles;
-(NSArray *)topUpCookies;
-(NSSet *)shuffle;
-(NSSet *)removeMatches;
-(Cookie *)cookieAtColumn:(NSInteger)column row:(NSInteger)row;
-(instancetype)initWithFile:(NSString *)fileName;
-(Tile *)tileAtColumn:(NSInteger)column row:(NSInteger)row;
-(void)performSwap:(Swap *)swap;
-(BOOL)isPossibleSwap:(Swap*)swap;
-(void)detectPossibleSwaps;
-(void)resetComboMultiplier;

@end
