//
//  Chain.m
//  CookieCrunch
//
//  Created by Ryan Salton on 21/10/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "Chain.h"

@implementation Chain {
    NSMutableArray *_cookies;
}

-(void)addCookie:(Cookie *)cookie
{
    if(_cookies == nil) {
        _cookies = [NSMutableArray array];
    }
    [_cookies addObject:cookie];
}

-(NSArray *)cookies
{
    return _cookies;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"type:%ld cookies:%@", (long)self.chainType, self.cookies];
}

@end
