//
//  Chain.h
//  CookieCrunch
//
//  Created by Ryan Salton on 21/10/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <Foundation/Foundation.h>
@class  Cookie;

typedef NS_ENUM(NSUInteger, ChainType) {
    ChainTypeHorizontal,
    ChainTypeVertical,
};

@interface Chain : NSObject

@property (nonatomic, strong, readonly) NSArray *cookies;
@property (nonatomic, assign) ChainType chainType;
@property (nonatomic, assign) NSUInteger score;

-(void)addCookie:(Cookie*)cookie;

@end
