//
//  GameViewController.m
//  CookieCrunch
//
//  Created by Ryan Salton on 20/10/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"
#import "Level.h"

@interface GameViewController ()
@property (nonatomic, strong) Level *level;
@property (nonatomic, strong) GameScene *scene;

@property (nonatomic, assign) NSUInteger movesLeft;
@property (nonatomic, assign) NSUInteger score;

@property (nonatomic, strong) UILabel *targetLabel;
@property (nonatomic, strong) UILabel *targetLabelNumber;
@property (nonatomic, strong) UILabel *moveLabel;
@property (nonatomic, strong) UILabel *moveLabelNumber;
@property (nonatomic, strong) UILabel *scoreLabel;
@property (nonatomic, strong) UILabel *scoreLabelNumber;

@property (nonatomic, strong) UIImageView *gameOverPanel;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@end

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    SKView *skView = (SKView *)self.view;
    skView.multipleTouchEnabled = NO;
    
    self.scene = [GameScene sceneWithSize:skView.bounds.size];
    self.scene.scaleMode = SKSceneScaleModeAspectFill;
    
    self.level = [[Level alloc] initWithFile:@"Level_3"];
    self.scene.level = self.level;
    [self.scene addTiles];
    
    id block = ^(Swap *swap) {
        self.view.userInteractionEnabled = NO;
        if([self.level isPossibleSwap:swap]) {
            [self.level performSwap:swap];
            [self.scene animateSwap:swap completion:^{
                [self handleMatches];
            }];
        }
        else {
            [self.scene animateInvalidSwap:swap completion:^{
                self.view.userInteractionEnabled = YES;
            }];
        }
    };
    self.scene.swipeHandler = block;
    
    self.gameOverPanel = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LevelComplete"]];
    self.gameOverPanel.frame = CGRectMake(0, (self.view.frame.size.height * 0.5) - (self.gameOverPanel.frame.size.height * 0.5), self.view.frame.size.width, self.gameOverPanel.frame.size.height);
    
    self.gameOverPanel.hidden = YES;
    [skView presentScene:self.scene];
    
    [self createInterface];
    
    [self.view addSubview:self.gameOverPanel];
    
    [self beginGame];
}

- (void)showGameOver {
    self.gameOverPanel.hidden = NO;
    self.scene.userInteractionEnabled = NO;
    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideGameOver)];
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
}

- (void)hideGameOver {
    [self.view removeGestureRecognizer:self.tapGestureRecognizer];
    self.tapGestureRecognizer = nil;
    
    self.gameOverPanel.hidden = YES;
    self.scene.userInteractionEnabled = YES;
    
    [self beginGame];
}

-(void)createInterface
{
    self.moveLabel = [self createInterfaceHeaderAtX:(self.view.frame.size.width * 0.5) - 45 withText:@"Moves:"];
    [self.view addSubview:self.moveLabel];
    
    self.moveLabelNumber = [self createInterfaceNumberAtX:self.moveLabel.frame.origin.x withText:@"999999"];
    [self.view addSubview:self.moveLabelNumber];
    
    self.targetLabel = [self createInterfaceHeaderAtX:self.moveLabel.frame.origin.x - self.moveLabel.frame.size.width - 10 withText:@"Target:"];
    [self.view addSubview:self.targetLabel];
    
    self.targetLabelNumber = [self createInterfaceNumberAtX:self.targetLabel.frame.origin.x withText:@"999999"];
    [self.view addSubview:self.targetLabelNumber];
    
    self.scoreLabel = [self createInterfaceHeaderAtX:self.moveLabel.frame.origin.x + self.moveLabel.frame.size.width + 10 withText:@"Score:"];
    [self.view addSubview:self.scoreLabel];
    
    self.scoreLabelNumber = [self createInterfaceNumberAtX:self.scoreLabel.frame.origin.x withText:@"999999"];
    [self.view addSubview:self.scoreLabelNumber];
}

-(UILabel *)createInterfaceHeaderAtX:(NSInteger)x withText:(NSString *)text
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, 20, 90, 30)];
    label.text = text;
    label.font = [UIFont fontWithName:@"GillSans-Bold" size:14.0];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.layer.shadowColor = [UIColor blackColor].CGColor;
    label.layer.shadowOffset = CGSizeMake(0, 2);
    label.layer.shadowRadius = 1.0;
    label.layer.shadowOpacity = 0.8;
    return label;
}

-(UILabel *)createInterfaceNumberAtX:(NSInteger)x withText:(NSString *)text
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, 50, 90, 30)];
    label.text = text;
    label.font = [UIFont fontWithName:@"GillSans-Bold" size:20.0];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.layer.shadowColor = [UIColor blackColor].CGColor;
    label.layer.shadowOffset = CGSizeMake(0, 2);
    label.layer.shadowRadius = 1.0;
    label.layer.shadowOpacity = 0.8;
    return label;
}

-(void)updateLabels
{
    self.targetLabelNumber.text = [NSString stringWithFormat:@"%lu", (long)self.level.targetScore];
    self.moveLabelNumber.text = [NSString stringWithFormat:@"%lu", (long)self.movesLeft];
    self.scoreLabelNumber.text = [NSString stringWithFormat:@"%lu", (long)self.score];
}

-(void)beginGame
{
    self.movesLeft = self.level.maximumMoves;
    self.score = 0;
    [self updateLabels];
    [self.level resetComboMultiplier];
    [self shuffle];
}

-(void)shuffle
{
    NSSet *newCookies = [self.level shuffle];
    [self.scene addSpritesForCookies:newCookies];
}

-(void)handleMatches
{
    NSSet *chains = [self.level removeMatches];
    if([chains count] == 0){
        [self beginNextTurn];
        return;
    }
    [self.scene animateMatchedCookies:chains completion:^{
        
        for(Chain *chain in chains) {
            self.score += chain.score;
        }
        [self updateLabels];
        
        NSArray *columns = [self.level fillHoles];
        [self.scene animateFallingCookies:columns completion:^{
            NSArray *columns = [self.level topUpCookies];
            [self.scene animateNewCookies:columns completion:^{
                [self handleMatches];
            }];
        }];
    }];
}

-(void)beginNextTurn
{
    [self decrementMoves];
    [self.level resetComboMultiplier];
    [self.level detectPossibleSwaps];
    self.view.userInteractionEnabled = YES;
}

-(void)decrementMoves
{
    self.movesLeft--;
    [self updateLabels];
    
    if (self.score >= self.level.targetScore) {
        self.gameOverPanel.image = [UIImage imageNamed:@"LevelComplete"];
        [self showGameOver];
    } else if (self.movesLeft == 0) {
        self.gameOverPanel.image = [UIImage imageNamed:@"GameOver"];
        [self showGameOver];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
