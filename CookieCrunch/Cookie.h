//
//  Cookie.h
//  CookieCrunch
//
//  Created by Ryan Salton on 20/10/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

@import SpriteKit;

static const NSUInteger NumCookieTypes = 6;

@interface Cookie : NSObject

@property (assign, nonatomic) NSInteger column;
@property (assign, nonatomic) NSInteger row;
@property (assign, nonatomic) NSInteger cookieType;
@property (assign, nonatomic) SKSpriteNode *sprite;

-(NSString *)spriteName;
-(NSString *)highlightedSpriteName;

@end
