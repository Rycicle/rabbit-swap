//
//  GameScene.h
//  CookieCrunch
//

//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class Level;
@class Swap;

@interface GameScene : SKScene

@property (nonatomic, copy) void (^swipeHandler) (Swap *swap);
@property (nonatomic, strong) Level *level;

-(void)animateSwap:(Swap *)swap completion:(dispatch_block_t)completion;
-(void)animateInvalidSwap:(Swap *)swap completion:(dispatch_block_t)completion;
-(void)animateMatchedCookies:(NSSet *)chains completion:(dispatch_block_t)completion;
- (void)animateFallingCookies:(NSArray *)columns completion:(dispatch_block_t)completion;
- (void)animateNewCookies:(NSArray *)columns completion:(dispatch_block_t)completion;
-(void)addSpritesForCookies:(NSSet *)cookies;
-(void)addTiles;

@end
